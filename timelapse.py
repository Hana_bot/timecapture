from importlib.resources import path
import time
from pathlib import Path
from tkinter import image_names
from datetime import datetime, timedelta
import winsound
import math
import yaml

import cv2
import imageio
import os


class Pictures:
    def __init__(self):

        self.dat = self.load_config()
        self.cam = cv2.VideoCapture(self.dat['camera']['port'][self.dat['camera']['choice']])
        self.parent_save_dir = Path(self.dat['parent_save_dir']).absolute()
        self.video_dir = Path(self.dat['save_dir']).parent / self.dat['video_dir']
        self.save_dir = self.parent_save_dir / Path(self.dat['save_dir'])
        self.make_parent_picture_dir()

    def load_config(self) -> dict:
        """
        Load the config file into the dat property

        Returns:
            Dictionary from yaml file.
        Fucks with:
            self.dat property.
        """
        config_path = Path.cwd() / 'config.yml'
        self.dat = yaml.safe_load(config_path.read_text())
        return self.dat

    def make_parent_picture_dir(self):
        """
        Make a parent directory for the picture directories if it doesn't already exist.
        """
        self.load_config()
        if self.parent_save_dir.is_dir():
            print("adding to existing parent directory")
            return
        self.parent_save_dir.mkdir()

    def make_picture_dir(self):
        """
        Make a directory for the pictures if it doesn't already exist.
        """
        self.load_config()
        if self.save_dir.is_dir():
            print("adding to existing photo directory")
            return
        self.save_dir.mkdir()

    def make_video_dir(self):
        """
        Make a directory for the videos if it doesn't already exist.
        """
        self.load_config()
        if self.video_dir.is_dir():
            print("adding to existing video directory")
            return
        self.video_dir.mkdir()

    def take_pictures(self):
        img_counter = 0

        # period between frames in seconds (Add in logic to handle units other
        # than seconds if it becomes relevant, but it's a little complicated with
        # an external config file...)
        timer_frequency = 1  # period to update terminal status in seconds

        period = self.dat['period']
        frames: int = int(self.dat['frames'])

        for _ in range(frames):  # desired number of images to capture
            did_beep = False
            ret, frame = self.cam.read()
            if not ret:
                print("failed to grab frame")

            pad = f"0{str(len(str(frames)))}"

            img_name = f"{self.dat['file_prefix']}_{img_counter:{pad}}.png"  # file names of resulting images

            img_name = self.save_dir / img_name
            img_name = str(img_name)
            this_time = datetime.now()
            cv2.imwrite(img_name, frame)
            print(f"{img_name} written at {this_time}!")

            time_to_next_image = time.time() + period
            while time.time() < time_to_next_image:
                time.sleep(timer_frequency)
                self.load_config()
                time_left = timedelta(seconds=time_to_next_image - time.time())
                print(f"Time until next image: {str(time_left)}", end='\r')

                if time_left.seconds <= self.dat['beep_boop']['period']:
                    if not did_beep:
                        time_to_next_image = time_to_next_image - self.beep()

                        did_beep = True

            img_counter += 1

    def beep(self) -> int:
        """
        Does make a beeping

        Return:
            amount of time beeping happened
        """
        self.load_config()
        if not self.dat['beep_boop']['do_it']:
            print("not gonna do it...")
            return 0
        print("commence beeping \n")
        hertz = 1000
        duration_ms = 500
        pause_ms  = 50  # time between beeps
        beep_zone = self.dat['beep_boop']['period']

        beeps_allowed = int(math.ceil(beep_zone / ((duration_ms + pause_ms) / 1000)))
        print(f"Beeping {beeps_allowed} times.")
        for _ in range(beeps_allowed):
            winsound.Beep(hertz, duration_ms)
            time.sleep(pause_ms / 1000)

        return beep_zone

    def take(self):
        try:
            self.make_picture_dir()
            self.take_pictures()
        except KeyboardInterrupt:
            print("\nJob done")
            self.cam.release()

    def do_make_mp4(self, mp4_name = None):
        """
        Make an mp4 why not
        """
        self.make_video_dir()
        if not mp4_name:
            mp4_name = self.save_dir.name
        writer = imageio.get_writer(f"{Path(self.video_dir) / mp4_name}{'.mp4'}", fps=20)
        if not self.save_dir.is_dir():
            raise OSError(f'No such directory: {self.save_dir}')

        pix = list(self.save_dir.glob('*.png'))
        pix.sort(key=os.path.getmtime)

        for thing in pix:
            print(thing)

        for im_file in pix:
            im = imageio.imread(im_file)
            dsize = (640, 368) # desired image size in pixels  height, width
            resized = cv2.resize(im, dsize, interpolation = cv2.INTER_AREA) #resizing image to be divisible by macro_block_size=16
            writer.append_data(resized)

        print("video done!")
        writer.close()

if __name__ == "__main__":
    thing = Pictures()
    thing.take()
    thing.do_make_mp4()
